$.fn.listHandlers = function(events, outputFunction) {
    return this.each(function(i){
        var elem = this,
            dEvents = $(this).data('events');
        if (!dEvents) {return;}
        $.each(dEvents, function(name, handler){
            if((new RegExp('^(' + (events === '*' ? '.+' : events.replace(',','|').replace(/^on/i,'')) + ')$' ,'i')).test(name)) {
               $.each(handler, function(i,handler){
                   outputFunction(elem, '\n' + i + ': [' + name + '] : ' + handler );
               });
           }
        });
    });
};

$.extend({
    parseJSON: function( data ) {
        if ( typeof data !== "string" || !data ) {
            return null;
        }    
        data = jQuery.trim( data );    
        if ( /^[\],:{}\s]*$/.test(data.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@")
            .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]")
            .replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ) {    
            return window.JSON && window.JSON.parse ?
                window.JSON.parse( data ) :
                (new Function("return " + data))();    
        } else {
            jQuery.error( "Invalid JSON: " + data );
        }
    }
});

Drupal.behaviors.getpost = function() {
  if ($.browser.msie) {
  var searchData = 
    function(element,data){
      if (data.indexOf('ajaxSubmit') > 0) {
        ajaxPost = true;
      }
    };
  $('form').submit(function(e) {
    var ajaxPost = false;
    var formAction = $(this).attr('action');
    $(this).listHandlers('submit',searchData);
    if (!ajaxPost) {
      var formdata = $(this).serialize();
      if (formdata.length > 20) {
        $.ajax({
          type: 'POST',
          url: '/getpost',
          data: $(this).serialize(),
          success: function(data) {
            var obj = $.parseJSON(data);
            window.location = formAction + '/?getpost=' + obj.key;
          }
        });
        e.preventDefault();
        return false;
      }
    }
  });
  }
};